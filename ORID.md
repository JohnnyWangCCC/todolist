### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we learned React Redux in the course, through Redux to simplify the difficulty of page data manipulation; in the afternoon we carried out demo exercises, and through the needs of multiple assignments for the reconstruction of the project; and then the teacher analyzed in detail the difference between React and VUe, as well as the advantages and disadvantages; front-end test writing attempts. The usage of Redux is indeed different from the usage of related technology frameworks in the past, and the unique writing style of React highlights the extreme flexibility, which has left a deep impression on me.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Bustling.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	Through the exposure to different front-end technology frameworks, it makes me have a deeper understanding of the technology; through the different focus and advantages and disadvantages, I no longer discuss who is better, but study which technology is more relevant to the team's difficulties.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Today, as a starter, there is less to learn, but there are gains to be made. I've had less exposure to React in the past, but I also recognize that React is a great tool for large projects.
