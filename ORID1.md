### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we had our usual stand meeting, then we learned React Router in the morning, and practiced demos, then in the afternoon we had Frontend Call APl, and refactored the project by iterating through several requirements; then the teacher introduced antd, and front-end related technologies such as SSR, etc.; then we used antd to beautify the project. Then the project is beautified using antd. In the practice of React Todos case, found a lot and gained a lot, especially React and other front-end frameworks, focusing on the runtime, can not be given in the compiler to analyze the specific problems, resulting in a lot of time spent in the tiny bugs.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Bustling.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	Through the exposure to different front-end technology frameworks, it makes me have a deeper understanding of the technology; through the different focus and advantages and disadvantages, I no longer discuss who is better, but study which technology is more relevant to the team's difficulties.At the same time, in the process of debugging bugs, I gained the ability to analyze problems in a lot of details, which is very important to me.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Today, as a starter, there is less to learn, but there are gains to be made. I've had less exposure to React in the past, but I also recognize that React is a great tool for large projects.I also gained a lot of experience in debugging bugs.
