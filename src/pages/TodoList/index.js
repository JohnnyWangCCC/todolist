import TodoItem from "../../components/TodoItem";
import { useSelector } from "react-redux"
import { useState } from "react";
import { useEffect } from 'react';
import useTodos from "../../hooks/useTodos";
import { Input } from 'antd';
import './index.css'
const { Search } = Input;

const Index = () => {
    const { getTodoList, createTodo } = useTodos()
    useEffect(() => {
        getTodoList()
    }, [])
    const todoList = useSelector(state => state.todo.todoList)
    const [AddTodoInfo, setAddTodoInfo] = useState('')
    const handleAddTodo = async () => {
        if (AddTodoInfo.length === 0) {
            alert("Invalid input")
            return
        }
        createTodo(AddTodoInfo)
    }

    const handleInputOnchange = (e) => {
        setAddTodoInfo(e.target.value)
    }

    return (
        <div className='todoListFit'>
            <h2>
                Todo List
            </h2>
            {todoList.map((tidoItem) => <TodoItem value={tidoItem} key={tidoItem.id}></TodoItem>)}
            <div>
                <Search
                    placeholder="input text"
                    enterButton="Add"
                    size="large"
                    onChange={handleInputOnchange}
                    onSearch={handleAddTodo}
                    className='todoAddStyle'
                />
            </div>
        </div>
    )
}

export default Index