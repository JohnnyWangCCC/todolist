import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import {Input} from 'antd';
import './index.css'
const {Search} = Input;

const DoneListPage = () => {
    const doneTodoList = useSelector(state => state.todo.todoList).filter(todo => todo.done === true)
    console.log(useSelector(state => state.todo.todoList),doneTodoList)

    const navigate = useNavigate()
    const handleClick = (id) => {
        navigate(`/doneList/${id}`)
    }

    return (
        doneTodoList.map((todoItem) => (
            <div key={todoItem.id}>
                <Search
                    className='doneItem'
                    value={todoItem.text}
                    placeholder="input text"
                    enterButton="Detail"
                    size="large"
                    onSearch={() => handleClick(todoItem.id)}
                />
            </div>
        ))
    )
}

export default DoneListPage