import { useParams } from "react-router-dom"
import { useSelector } from "react-redux"
import { Card } from 'antd';
import './index.css'
import { useEffect, useState } from "react";
import { getTodoById } from "../../apis/todoApis";

const TodoDetailPage = () => {
    const { id } = useParams("id")
    const [todo, setTodo] = useState({})
    // const loadTodoData = async () => {
    //     const { data } =  getTodoById(id)
    //     setTodo(data)
    // }

    const loadTodoData = async () => {
        const {data} = await getTodoById(id)
        setTodo(data)
    }

    useEffect(() => {
        loadTodoData()
    },[])
    return (
        <div>
            <h3>Detail Page</h3>
            <Card
                title='Detail CardInfo'
                bordered
                className='card'
            >
                <p>{todo.text}</p>
            </Card>
        </div>
    )
}

export default TodoDetailPage