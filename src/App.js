import {
    MailOutlined, HomeOutlined, UnorderedListOutlined
} from '@ant-design/icons';
import {Outlet, useNavigate} from 'react-router-dom';
import './App.css';
import {useState} from 'react';
import {Layout, Menu} from 'antd';
import useBreakpoint from "antd/es/grid/hooks/useBreakpoint";

const {Sider, Header} = Layout;

function App() {

    function getItem(label, key, icon, children, type) {
        return {
            key,
            icon,
            children,
            label,
            type,
        };
    }

    const items = [
        getItem('Home Page', '/', <HomeOutlined/>),
        getItem('DoneList Page', '/doneList', <UnorderedListOutlined/>),
        getItem('Help Page', '/help', <MailOutlined/>),
    ];
    const [collapsed, setCollapsed] = useState(false);
    const screens = useBreakpoint();
    const navigate = useNavigate()
    const handleMenuOnClick = (e) => {
        navigate(e.key, {replace: true})
    }
    const siderProps = {
        trigger: null,
        collapsible: true,
        collapsed: collapsed,
        style: {
            backgroundColor: 'white',
            minWidth: 50,
        },
    };

    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
    };

    if (screens.md || screens.lg) {
        siderProps.width = 200;
        siderProps.collapsedWidth = 80;
    } else {
        siderProps.width = '20%';
    }

    const menuProps = {
        onClick: handleMenuOnClick,
        items: items,
    };

    return (
        <div className="App" style={{
            padding: 0,
            backgroundColor: 'white'
        }}>
            <Layout>
                <Sider {...siderProps} onCollapse={toggleCollapsed} style={{
                    width: 256,
                    backgroundColor: 'white'
                }}>
                    <Menu
                        {...menuProps}
                    >
                    </Menu>
                </Sider>
                <Layout style={{
                    padding: 0,
                    backgroundColor: 'white'
                }}>
                    <Header
                        style={{
                            padding: 0,
                            backgroundColor: 'white'
                        }}
                    >
                        <Outlet></Outlet>

                    </Header>

                </Layout>
            </Layout>
        </div>
    );
}

export default App;
