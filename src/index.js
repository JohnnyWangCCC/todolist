import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux'
import store from './store'
import Help from './pages/HelpPage'
import NotFoundPage from './pages/NotFoundPage'
import DoneListPage from './pages/DoneListPage'
import Index from './pages/TodoList';
import TodoDetailPage from './pages/TodoDetailPage';

import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom'

const router = createBrowserRouter([
  {
    path: "/",
    element: <App></App>,
    children: [
      {
        index: true,
        element: <Index></Index>
      },
      {
        path: "/doneList",
        element: <DoneListPage></DoneListPage>
      },
      {
        path: "/doneList/:id",
        element: <TodoDetailPage></TodoDetailPage>
      },
      {
        path: "/help",
        element: <Help></Help>
      },
    ]
  },
  {
    path: "/*",
    element: <NotFoundPage></NotFoundPage>
  }
])

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}>
        <App />
      </RouterProvider>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
