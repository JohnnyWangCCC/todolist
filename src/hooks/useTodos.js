import { useDispatch } from "react-redux"
import {getTodos, createTodos, deleteTodos, updateTodosDone, updateTodosInput} from "../apis/todoApis"
import { loadTodos } from "../components/todoSlice"

const useTodos = () => {
    const dispatch = useDispatch()

    const getTodoList = async () => {
        const {data} = await getTodos()
        dispatch(loadTodos(data))
    }

    const createTodo = async (text) => {
        await createTodos({"text": text, "done": false })
        getTodoList()
    }

    const deleteTodo = async (id) => {
        await deleteTodos(id)
        getTodoList()
    }

    const updateTodo = async (id, data) => {
        await updateTodosDone(id, data)
        getTodoList()
    }

    const updateTodoInput = async (id, data) => {
        await updateTodosInput(id, data)
        getTodoList()
    }

    return {
        getTodoList,
        createTodo,
        deleteTodo,
        updateTodo,
        updateTodoInput
    }
}

export default useTodos