import axios from "axios"

const mockapi = axios.create({
    baseURL: 'https://64c0b6680d8e251fd112640e.mockapi.io/api'
})

export default mockapi
