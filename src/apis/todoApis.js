import api from "./api"
import mockapi from "./mockapi"

export const getTodos = () => {
    return api.get('/todos')
}

export const deleteTodos = (id) => {
    return api.delete(`/todos/${id}`)
}

export const updateTodosDone = (id, data) => {
    return api.put(`/todos/${id}`, {
        id: id,
        done: data
    })
}

export const updateTodosInput = (id, data) => {
    return api.put(`/todos/${id}`, {
        id: id,
        text: data
    })
}

export const createTodos = (data) => {
    return api.post(`/todos`, data)
}

export const getTodoById = (id) => {
    return api.get(`/todos/${id}`)
}

export const todoApis = {
    getTodos,
    createTodos,
    deleteTodos,
    updateTodosDone, 
    updateTodosInput
}