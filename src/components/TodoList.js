import TodoItem from "./TodoItem";
import { useSelector } from "react-redux"
import { useState } from "react";
import { useDispatch } from 'react-redux'
import { addNew, loadTodos } from './todoSlice'
import { useEffect } from 'react';
import { createTodos, getTodos } from '../apis/mockapi';
import useTodos from "../hooks/useTodos";

const TodoList = () => {
    const { getTodoList, createTodo } = useTodos()
    useEffect(() => {
        getTodoList()
    }, [])
    const todoList = useSelector(state => state.todo.todoList)
    const [AddTodoInfo, setAddTodoInfo] = useState('')
    const handleAddTodo = async () => {
        if (AddTodoInfo.length === 0) {
            alert("Invalid input")
            return
        }
        createTodo(AddTodoInfo)
    }

    const handleInputOnchange = (e) => {
        setAddTodoInfo(e.target.value)
    }

    return (
        <div>
            <header>
                Todo List
            </header>
            {todoList.map((tidoItem) => <TodoItem value={tidoItem} key={tidoItem.id}></TodoItem>)}
            <div>
                <input onChange={handleInputOnchange} />
                <button onClick={handleAddTodo} className="addButton">add</button>
            </div>
        </div>
    )
}

export default TodoList