import { Link } from 'react-router-dom'

const Navigators = () => {
    return (
        <div>
            <ul>
                <li>
                    <Link to="/">Home Page</Link>
                </li>
                <li>
                    <Link to="/doneList">DoneList Page</Link>
                </li>
                <li>                    
                    <Link to="/help">Help Page</Link>
                </li>
            </ul>
        </div>
    )
}

export default Navigators