import '../style/todoItemStyle.css'
import useTodos from '../hooks/useTodos'
import {Input} from 'antd';
import {CloseOutlined, CheckOutlined, EditOutlined} from '@ant-design/icons';
import {useState} from "react";

const TodoItem = ({value}) => {
    const {deleteTodo, updateTodo, updateTodoInput} = useTodos()
    const textDecorationStyle = {width: "200px", margin: "5px 10px 5px", textDecoration: "line-through"}
    const noneTextDecorationStyle = {width: "300px", margin: "5px 10px 5px"}
    const [inputUpdate, setInputUpdate] = useState(value.text)
    const handleFinishOnClick = async () => {
        await updateTodo(value.id, !value.done)
    }

    const handleDeleteOnClick = async () => {
        await deleteTodo(value.id)
    }

    const handleEditOnClick = async () => {
        await updateTodoInput(value.id, inputUpdate)
    }

    const onChange = (e) => {
        console.log(e.target.value)
        setInputUpdate(e.target.value)
    }

    const suffix = (
        <div>
            <CloseOutlined
                onClick={handleDeleteOnClick}
                style={{
                    fontSize: 16,
                    color: '#1677ff',
                }}
            />
            <EditOutlined
                onClick={handleEditOnClick}
                style={{
                    fontSize: 16,
                    color: '#1677ff',
                }}
            />
        </div>
    );

    const prefix = (
        <CheckOutlined
            onClick={handleFinishOnClick}
            style={{
                fontSize: 16,
                color: '#1677ff',
            }}
        />
    );

    return (
        <Input
            onChange={onChange}
            prefix={prefix}
            value={inputUpdate}
            placeholder="input text"
            size="large"
            suffix={suffix}
            style={value.done === true ? textDecorationStyle : noneTextDecorationStyle}
        />
    )
}

export default TodoItem