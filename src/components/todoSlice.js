import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: [{ "id": 1, "text": "text", "done": true }]
    },
    reducers: {
        addNew: (state, action) => {
            state.todoList.push(action.payload)
        },
        deleteItem: (state, action) => {
            state.todoList = state.todoList.filter(todo => action.payload != todo.id)
        },
        updateTodoStatus: (state, action) => {
            state.todoList.find(todo => action.payload == todo.id).done = !state.todoList.find(todo => action.payload == todo.id).done;
        },
        loadTodos: (state, action) => {
            state.todoList = action.payload
        }
    },
})

export const { addNew, deleteItem, updateTodoStatus, loadTodos } = todoSlice.actions

export default todoSlice.reducer